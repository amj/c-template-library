#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "template.h"

#ifndef CLASS_NAME
#define LOCAL_CLASS
#define CLASS_NAME CircularLinkedList
#endif

#define LIST_ELEM_CLASS_NAME CONCAT_MACRO(CLASS_NAME, Elem)

#define LIST_CLASS CLASS_TEMPLATE
#define LIST_METHOD METHOD_TEMPLATE

#define LIST_ELEM_CLASS TYPE_TEMPLATE(LIST_ELEM_CLASS_NAME)
#define LIST_ELEM_METHOD(name) FUNC_TEMPLATE(LIST_ELEM_CLASS_NAME, name)

typedef struct LIST_ELEM_CLASS {
    struct LIST_ELEM_CLASS *next;
    struct LIST_ELEM_CLASS *prev;
    union {
        intptr_t size;                 // only use on head <=> odd
        struct LIST_ELEM_CLASS *head;  // pointer is always even
    };
    PRIVATE_TEMPLATE_TYPE data;
} LIST_ELEM_CLASS;

/**
 * for_each_elem - iterate over a list safe against removal of list entry
 * @type the type   of LIST_ELEM
 * @pos:»---the &struct list_head to use as a loop cursor.
 * @head:»--the head for your list.
 */
#define for_each_elem(type, pos, head)                             \
    for (type *pos = (head)->next, *_n = pos->next; pos != (head); \
         pos = _n, _n = pos->next)

static inline void LIST_METHOD(init_head)(LIST_ELEM_CLASS *head) {
    head->next = head;
    head->prev = head;
    head->size = 1;
#ifdef TEMPLATE_IS_POINTER
    head->data = NULL;
#endif
}

static inline int LIST_METHOD(is_head)(LIST_ELEM_CLASS *head) {
    return head->size % 2 == 1;  // pointer is always even
}

static inline LIST_ELEM_CLASS *LIST_ELEM_METHOD(get_head)(
    LIST_ELEM_CLASS *elem) {
    if (LIST_METHOD(is_head)(elem))
        return elem;
    else
        return elem->head;
}

static inline int LIST_METHOD(get_size)(LIST_ELEM_CLASS *head) {
    head = LIST_ELEM_METHOD(get_head)(head);
    return head->size / 2;
}

static inline int LIST_METHOD(is_empty)(LIST_ELEM_CLASS *head) {
    return head->next == head;
}

static inline TEMPLATE_TYPE *LIST_ELEM_METHOD(get_content)(
    LIST_ELEM_CLASS *elem) {
    //
#ifdef TEMPLATE_IS_PTR
    return elem->data;
#else
    return &elem->data;
#endif
}

static inline TEMPLATE_TYPE **LIST_METHOD(as_array)(LIST_ELEM_CLASS *head,
                                                    int *size) {
    int s;
    if (size == NULL) size = &s;
    head = LIST_ELEM_METHOD(get_head)(head);
    *size = LIST_METHOD(get_size)(head);
    int counter = 0;
    TEMPLATE_TYPE **array = malloc(sizeof(TEMPLATE_TYPE *) * *size);
    for_each_elem(LIST_ELEM_CLASS, elem, head) {
        if (*size != LIST_METHOD(get_size)(head))
            // incertion/remove while doing the loop
            exit(-1);
        array[counter] = LIST_ELEM_METHOD(get_content)(elem);
        counter++;
    }
    return array;
}

static inline LIST_ELEM_CLASS **LIST_METHOD(as_array_of_elem)(
    LIST_ELEM_CLASS *head, int *size) {
    int s;
    if (size == NULL) size = &s;
    head = LIST_ELEM_METHOD(get_head)(head);
    *size = LIST_METHOD(get_size)(head);
    int counter = 0;
    LIST_ELEM_CLASS **array = malloc(sizeof(LIST_ELEM_CLASS *) * *size);
    for_each_elem(LIST_ELEM_CLASS, elem, head) {
        if (*size != LIST_METHOD(get_size)(head))
            // incertion/remove while doing the loop
            exit(-1);
        array[counter] = elem;
        counter++;
    }
    return array;
}

static inline LIST_ELEM_CLASS *LIST_ELEM_METHOD(new)(
    PRIVATE_TEMPLATE_TYPE data) {
    LIST_ELEM_CLASS *elem_ptr = malloc(sizeof(LIST_ELEM_CLASS));
    if (elem_ptr == NULL) {
        perror("malloc");
        exit(-1);
    }
    elem_ptr->next = NULL;
    elem_ptr->prev = NULL;
    elem_ptr->head = NULL;
    elem_ptr->data = data;
    return elem_ptr;
}

static inline void LIST_ELEM_METHOD(remove)(LIST_ELEM_CLASS *elem) {
    if (elem->head != NULL) {
        elem->prev->next = elem->next;
        elem->next->prev = elem->prev;
        elem->head->size -= 2;
    } else if (elem->next != NULL || elem->prev != NULL)  // inconsistent state
        exit(-1);
    else if (LIST_METHOD(is_head)(elem))  // can't be headless
        exit(-1);

    elem->next = NULL;
    elem->prev = NULL;
    elem->head = NULL;
}

// PRIVATE
static inline void PRIVATE(LIST_ELEM_METHOD(add_between))(
    LIST_ELEM_CLASS *new, LIST_ELEM_CLASS *prev, LIST_ELEM_CLASS *next) {
    LIST_ELEM_METHOD(remove)(new);
    new->head = LIST_ELEM_METHOD(get_head)(prev);
    new->head->size += 2;

    next->prev = new;
    new->next = next;
    new->prev = prev;
    prev->next = new;
}

static inline PRIVATE_TEMPLATE_TYPE LIST_ELEM_METHOD(delete)(
    LIST_ELEM_CLASS *elem) {
    PRIVATE_TEMPLATE_TYPE data = elem->data;
    LIST_ELEM_METHOD(remove)(elem);
    free(elem);
    return data;
}

static inline LIST_ELEM_CLASS *LIST_METHOD(add)(LIST_ELEM_CLASS *head,
                                                PRIVATE_TEMPLATE_TYPE to_add) {
    head = LIST_ELEM_METHOD(get_head)(head);
    LIST_ELEM_CLASS *elem_ptr = LIST_ELEM_METHOD(new)(to_add);
    PRIVATE(LIST_ELEM_METHOD(add_between))(elem_ptr, head, head->next);
    return elem_ptr;
}

static inline LIST_ELEM_CLASS *LIST_METHOD(add_tail)(
    LIST_ELEM_CLASS *head, PRIVATE_TEMPLATE_TYPE to_add) {
    head = LIST_ELEM_METHOD(get_head)(head);
    LIST_ELEM_CLASS *elem_ptr = LIST_ELEM_METHOD(new)(to_add);
    PRIVATE(LIST_ELEM_METHOD(add_between))(elem_ptr, head, head->next);
    return elem_ptr;
}

#ifdef LOCAL_CLASS
#undef LOCAL_CLASS
#undef CLASS_NAME
#endif


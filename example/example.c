#include "include/example_struct.h"
#include "include/circular_linked_list_of_example_struct.h"

int main() {
    printf("without pointer\n");
    CircularLinkedListElem_of_ExampleStruct head;
    CircularLinkedList_of_ExampleStruct__init_head(&head);
    ExampleStruct elem;
    elem.d = 42;
    printf("value of elem.d: %i \n", elem.d);
    CircularLinkedListElem_of_ExampleStruct* list_elem =
        CircularLinkedList_of_ExampleStruct__add(&head, elem);
    elem.d = 12;
    printf("value of elem.d: %i \n", elem.d);
    elem = CircularLinkedListElem_of_ExampleStruct__delete(list_elem);
    printf("value of elem.d: %i \n", elem.d);

    printf("with pointer\n");
    CircularLinkedListElem_of_ExampleStruct_ptr head2;
    CircularLinkedList_of_ExampleStruct_ptr__init_head(&head2);
    elem.d = 42;
    printf("value of elem.d: %i \n", elem.d);
    CircularLinkedListElem_of_ExampleStruct_ptr* list_elem2 =
        CircularLinkedList_of_ExampleStruct_ptr__add(&head2, &elem);
    elem.d = 12;
    printf("value of elem.d: %i \n", elem.d);
    elem = *CircularLinkedListElem_of_ExampleStruct_ptr__delete(list_elem2);
    printf("value of elem.d: %i \n", elem.d);
}


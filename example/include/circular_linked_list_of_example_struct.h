#ifndef CIRCULAR_LINKED_LIST_OF_EXAMPLE_STRUCT // include the template in an isolate file is a good practice to avoid multiple definitions
#define CIRCULAR_LINKED_LIST_OF_EXAMPLE_STRUCT

#include "example_struct.h"


#define TEMPLATE_TYPE ExampleStruct
#define TEMPLATE_IS_PTR
#include "../../include/circular_linked_list_template.h"

#undef TEMPLATE_IS_PTR
#include "../../include/circular_linked_list_template.h"

#endif
